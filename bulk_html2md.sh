#!/bin/bash
mkdir -p export;
##copy directories if exists
[ -d attachments ] && cp -r attachments export/;
[ -d images ] && cp -r images export/;
##conver html to md
for page in *.html; do
    touch export/$(echo $page|cut -d . -f1).md
    html2md -i $page > export/$(echo $page|cut -d . -f1).md 
                    done;
find ./export -type f -exec sed -i -e 's/.html/.md/g' {} \;
sed -i '/Space\ Details/,3d;' export/*.md;
sed -i 's/Document\ generated .*//g' export/*.md;
sed -i 's/Created .*//g' export/*.md;
sed -i 's/\[Atlassian.*//g' export/*.md;
exit 0
